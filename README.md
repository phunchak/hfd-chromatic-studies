# HFD Chromatic Studies
## Directories

### lattices:	
*Lattice files for MADX simulations* 

### plotting: 	
*Python plotting scripts*
		
**plot\_chroma\_check.py**:	Plot tunes (Q1 vs Q2) against the momentum offset dp, to display chromaticity for each simulated lattice.
	
**plot\_twiss\_chroma.py**:	Plot chromatic optics (betax and betay) for on-momentum and a few off-momentum cases for each simulated lattice.

### scripts: 	
*Python MADX and PTC calculation scripts*

**fitting\_chrom.py**: 		Fit a polynomial to the simulated chromaticity curve to determine dQ1, dQ2 etc., also includes option to plot fit.

**macro\_ptc\_chroma.madx**:	PTC macro to calculate higher order chromaticity terms, called by ptc\_chrom\_calc.py.
	
**ptc\_chrom\_calc.py**:	PTC script to calculate higher order chromaticity terms for each lattice.
	
**twiss\_chroma\_check**:	Calculate off-momenta twiss for each lattice and store first and second order chromaticity and optics parameters at the IP.

### HFD\_opt
*MAD-NG scripts to calculate and optimize the higher order chromatic terms*

### xsuite\_studies
*xsuite for calculation of amplitude dependent detuning and higher order chromaticities*

