import matplotlib.pyplot as plt
import numpy as np
import tfs
from cpymad.madx import Madx
import pandas as pd
import re
import math
from matplotlib.ticker import FormatStrFormatter
import os
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import json
import itertools


#load lattice parameters
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))
LATTICE = sorted([version+mode for version in REF_FILE['VERSION'].keys() for mode in REF_FILE['OPERATION_MODES'].keys()])



ngmad_result_folder = "../Outputdata/ngmad/"
#INCLUDE SOME WAY TO ENSURE NO EXTRANEOUS .swp files, causes issues later on
file_list = sorted(os.listdir(ngmad_result_folder))
print(file_list)
#xchroma_term_list   = ['q1_ref','dq1_dp1','dq1_dp2','dq1_dp3','dq1_dp4','dq1_dp5']
#ychroma_term_list   = ['q2_ref','dq2_dp1','dq2_dp2','dq2_dp3','dq2_dp4','dq2_dp5']
#chroma_terms        = xchroma_term_list+ychroma_term_list
#chroma_order        = list(rane(0,6,1)) + list(range(0,6,1))
chroma_order        = [0,0,1,2,3,4,5,1,2,3,4,5]
chroma_factorials   = [1/math.factorial(i) for i in chroma_order]

xanh_term_list  = ['dq1_djx1','dq1_djy1','dq1_djx1dp1','dq1_djx1djy1','dq1_djy1dp1','dq1_djx2','dq1_djy2']
yanh_term_list  = ['dq2_djx1','dq2_djy1','dq2_djx1dp1','dq2_djx1djy1','dq2_djy1dp1','dq2_djx2','dq2_djy2']
anh_terms       = xanh_term_list+yanh_term_list
anh_order       = [1,1,2,2,2,2,2,1,1,2,2,2,2,2]
anh_factorials  = [1/math.factorial(i) for i in anh_order]

chroma_latex_labels = ['$Q_x$',
                '$Q_y$',
                '$\\frac{\partial Q_x}{\partial p}$',
                '$\\frac{\partial^2 Q_x}{\partial p^2}$',
                '$\\frac{\partial^3 Q_x}{\partial p^3}$',
                '$\\frac{\partial^4 Q_x}{\partial p^4}$',
                '$\\frac{\partial^5 Q_x}{\partial p^5}$',
                '$\\frac{\partial Q_y}{\partial p}$',
                '$\\frac{\partial^2 Q_y}{\partial p^2}$',
                '$\\frac{\partial^3 Q_y}{\partial p^3}$',
                '$\\frac{\partial^4 Q_y}{\partial p^4}$',
                '$\\frac{\partial^5 Q_y}{\partial p^5}$'
                ]
anh_latex_labels = [
        '$\\frac{\partial Q_x}{\partial J_x}$',
        '$\\frac{\partial Q_x}{\partial J_y}$',
        '$\\frac{\partial^2 Q_x}{\partial J_x \partial p}$',
        '$\\frac{\partial^2 Q_x}{\partial J_x \partial J_y}$',
        '$\\frac{\partial^2 Q_x}{\partial J_y \partial p}$',
        '$\\frac{\partial^2 Q_x}{\partial J^{2}_x}$',
        '$\\frac{\partial^2 Q_x}{\partial J^{2}_y}$',
        '$\\frac{\partial Q_y}{\partial J_x}$',
        '$\\frac{\partial Q_y}{\partial J_y}$',
        '$\\frac{\partial^2 Q_y}{\partial J_x \partial p}$',
        '$\\frac{\partial^2 Q_y}{\partial J_x \partial J_y}$',
        '$\\frac{\partial^2 Q_y}{\partial J_y \partial p}$',
        '$\\frac{\partial^2 Q_y}{\partial J^{2}_x}$',
        '$\\frac{\partial^2 Q_y}{\partial J^{2}_y}$',
        ]

PLOTCHROM = 1 
PLOTANH   = 1

#right now it opens each file for each set of orders, would be better to open each file only one time and then store all the values in python to continue on, less file reading.

files_to_compare = [file_list[7],file_list[3]]
name1="V23 $Z$"
name2="HFD79 $Z$"

if PLOTCHROM == 1:
    values = np.empty((2,len(chroma_order)))
    print(values)
    i=0
    for file in files_to_compare:
        df = pd.read_csv(ngmad_result_folder+file,sep="\t",index_col=0,names=["Term","Value"])
        values[i] = chroma_factorials * df.Value[0:12].values
        
        print("Printing for")
        print(i)
        print(abs(values[i][:]))
        i=i+1
    

    xaxis = np.arange(len(chroma_order))
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif',weight=6,size=25)
    plt.figure(figsize=(12,9))
    plt.bar(xaxis-0.2,abs(values[0][:]),label=name1,width=0.4) #MAKE SURE THAT LATTICE lines up with file list, perhaps use file_list directly
    plt.bar(xaxis+0.2,abs(values[1][:]),label=name2,width=0.4) #MAKE SURE THAT LATTICE lines up with file list, perhaps use file_list directly
    plt.legend(loc="upper left")
    plt.yscale('log')
    plt.title("Chromaticity in "+name1+" and "+name2+" Lattices")
    plt.ylabel("$n^{th}$-Order Chromaticity, $[Dimensionless]$")
    plt.xticks(xaxis,chroma_latex_labels,rotation=0,)
    plt.xlabel("")
    plt.tight_layout()
    plt.savefig("../Outputdata/figures/ngmad/full/"+LATTICE[file_list.index(files_to_compare[0])]+"_"+LATTICE[file_list.index(files_to_compare[1])]+"_chroma_comparison.pdf",format=pdf)
    plt.close()

if PLOTANH == 1:
    values = np.empty((2,len(anh_order)))
    print(values)
    i=0
    for file in files_to_compare:
        df = pd.read_csv(ngmad_result_folder+file,sep="\t",index_col=0,names=["Term","Value"])
        values[i] = df.Value[12:26].values
        
        print("Printing for")
        print(i)
        print(abs(values[i][:]))
        i=i+1
    
    xaxis = np.arange(len(anh_order))
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif',weight=6,size=25)
    plt.figure(figsize=(16,8))
    plt.bar(xaxis-0.2,abs(values[0][:]),label=name1,width=0.4) #MAKE SURE THAT LATTICE lines up with file list, perhaps use file_list directly
    plt.bar(xaxis+0.2,abs(values[1][:]),label=name2,width=0.4) #MAKE SURE THAT LATTICE lines up with file list, perhaps use file_list directly
    plt.legend(loc="upper left")
    plt.yscale('log')
    plt.title("Anharmonicity in "+name1+" and "+name2+" Lattices")
    plt.ylabel("Anharmonicity, $\propto \\frac{1}{[m]}$ or $\\frac{1}{[m^2]}$ ")
    plt.xticks(xaxis,anh_latex_labels,rotation=0, fontsize=30)
    plt.xlabel("")
    plt.tight_layout()
    plt.savefig("../Outputdata/figures/ngmad/full/"+LATTICE[file_list.index(files_to_compare[0])]+"_"+LATTICE[file_list.index(files_to_compare[1])]+"_anh_comparison.pdf",format=pdf)
    plt.close()

