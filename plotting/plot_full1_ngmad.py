import matplotlib.pyplot as plt
import numpy as np
import tfs
from cpymad.madx import Madx
import pandas as pd
import re
import math
from matplotlib.ticker import FormatStrFormatter
import os
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import json
import itertools
#load lattice parameters
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))
LATTICE = sorted([version+mode for version in REF_FILE['VERSION'].keys() for mode in REF_FILE['OPERATION_MODES'].keys()])



ngmad_result_folder = "../Outputdata/ngmad/"
#INCLUDE SOME WAY TO ENSURE NO EXTRANEOUS .swp files, causes issues later on
file_list = sorted(os.listdir(ngmad_result_folder))

#xchroma_term_list   = ['q1_ref','dq1_dp1','dq1_dp2','dq1_dp3','dq1_dp4','dq1_dp5']
#ychroma_term_list   = ['q2_ref','dq2_dp1','dq2_dp2','dq2_dp3','dq2_dp4','dq2_dp5']
#chroma_terms        = xchroma_term_list+ychroma_term_list
#chroma_order        = list(range(0,6,1)) + list(range(0,6,1))
chroma_order        = [0,0,1,2,3,4,5,1,2,3,4,5]
chroma_factorials   = [1/math.factorial(i) for i in chroma_order]

xanh_term_list  = ['dq1_djx1','dq1_djy1','dq1_djx1dp1','dq1_djx1djy1','dq1_djy1dp1','dq1_djx2','dq1_djy2']
yanh_term_list  = ['dq2_djx1','dq2_djy1','dq2_djx1dp1','dq2_djx1djy1','dq2_djy1dp1','dq2_djx2','dq2_djy2']
anh_terms       = xanh_term_list+yanh_term_list
anh_order       = [1,1,2,2,2,2,2,1,1,2,2,2,2,2]
anh_factorials  = [1/math.factorial(i) for i in anh_order]

PLOTCHROM = 0
PLOTANH   = 1

#right now it opens each file for each set of orders, would be better to open each file only one time and then store all the values in python to continue on, less file reading.

if PLOTCHROM == 1:
    for file in file_list:
        seq_curr = LATTICE[file_list.index(file)]
        print("------------------------\n"+seq_curr)
        values = np.empty(len(file_list))

        df = pd.read_csv(ngmad_result_folder+file,sep="\t",index_col=0,names=["Term","Value"])
        print(df.Value[0:12])
        print(chroma_factorials)
        values = chroma_factorials * df.Value[0:12].values

        print(values)
        plt.bar(df.index[0:12],abs(values),width=0.5) #MAKE SURE THAT LATTICE lines up with file list, perhaps use file_list directly
        plt.yscale('log')
        plt.title(seq_curr)
        plt.ylabel("chromaticity")
        plt.xticks(rotation=45)
        plt.xlabel("Lattice")
        plt.tight_layout()
        plt.savefig("../Outputdata/figures/ngmad/full/"+seq_curr+"_chroma.png")
        plt.close()


if PLOTANH == 1:
    for file in file_list:
        seq_curr = LATTICE[file_list.index(file)]
        print("------------------------\n"+seq_curr)
        values = np.empty(len(file_list))

        df = pd.read_csv(ngmad_result_folder+file,sep="\t",index_col=0,names=["Term","Value"])
        
        values = df.Value[12:26].values
        print(values)
        plt.bar(df.index[12:26],abs(values),width=0.5) #MAKE SURE THAT LATTICE lines up with file list, perhaps use file_list directly
        plt.yscale('log')
        plt.title(seq_curr)
        plt.ylabel("anharmonicity")
        plt.xticks(rotation=45)
        plt.xlabel("Lattice")
        plt.tight_layout()
        plt.savefig("../Outputdata/figures/ngmad/full/"+seq_curr+"_anh.png")
        plt.close()

#if PLOTCHROM == 1:
#    for term in chroma_terms:
#        print("------------------------\n"+term)
#        order = chroma_order[chroma_terms.index(term)]
#        values = np.empty(len(file_list))
#
#        for file in file_list:
#            df = pd.read_csv(ngmad_result_folder+file,sep="\t",index_col=0,names=["Term","Value"])
#            values[file_list.index(file)] = (1/math.factorial(order)) * df.loc[term].Value
#        print(values)
#        plt.bar(LATTICE,abs(values),width=0.5) #MAKE SURE THAT LATTICE lines up with file list, perhaps use file_list directly
#        plt.yscale('log')
#        plt.title(term)
#        plt.ylabel(str(order)+"-order chromaticity")
#        plt.xticks(rotation=45)
#        plt.xlabel("Lattice")
#        plt.tight_layout()
#        plt.savefig("../Outputdata/figures/ngmad/"+term+"_chroma.png")
#        plt.close()
#
#if PLOTANH ==1:
#    for term in anh_terms:
#        print("------------------------\n"+term)
#        values = np.empty(len(file_list))
#
#        for file in file_list:
#            df = pd.read_csv(ngmad_result_folder+file,sep="\t",index_col=0,names=["Term","Value"])
#            values[file_list.index(file)] = df.loc[term].Value
#        print(values)
#        plt.bar(LATTICE,abs(values),width=0.5)
#        plt.yscale('log')
#        plt.title(term)
#        plt.ylabel(term+" anharmonicity")
#        plt.xticks(rotation=45)
#        plt.xlabel("Lattice")
#        plt.tight_layout()
#        plt.savefig("../Outputdata/figures/ngmad/"+term+"_anharmon.png")
#        plt.close()
