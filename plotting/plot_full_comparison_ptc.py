import matplotlib.pyplot as plt
import numpy as np
import tfs
from cpymad.madx import Madx
import pandas as pd
import re
import math
from matplotlib.ticker import FormatStrFormatter
import os
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import json
import itertools
#load lattice parameters
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))
LATTICE = sorted([version+mode for version in REF_FILE['VERSION'].keys() for mode in REF_FILE['OPERATION_MODES'].keys()])



ptc_result_folder = "../Outputdata/ptc_rad/"
#INCLUDE SOME WAY TO ENSURE NO EXTRANEOUS .swp files, causes issues later on
file_list = sorted(os.listdir(ptc_result_folder))


order_col = ["ORDER1","ORDER2","ORDER3"]
orders = [1,2,3,4,5]
anh_combinations = [[1,0,0],[0,1,0],[1,1,0],[1,0,1],[0,1,1],[1,1,1],[2,0,0],[0,2,0]]

tune_names = ["DQ1","DQ2"]
anh_names  = ["ANHX","ANHY"]

xchroma_term_list   = ['q1_ref','dq1_dp1','dq1_dp2','dq1_dp3','dq1_dp4','dq1_dp5']
ychroma_term_list   = ['q2_ref','dq2_dp1','dq2_dp2','dq2_dp3','dq2_dp4','dq2_dp5']
chroma_terms        = xchroma_term_list+ychroma_term_list
chroma_order        = list(range(0,6,1)) + list(range(0,6,1))

xanh_term_list  = ['dq1_djx1','dq1_djy1','dq1_djx1dp1','dq1_djx1djy1','dq1_djy1dp1','dq1_djx2','dq1_djy2']
yanh_term_list  = ['dq2_djx1','dq2_djy1','dq2_djx1dp1','dq2_djx1djy1','dq2_djy1dp1','dq2_djx2','dq2_djy2']
anh_terms       = xanh_term_list+yanh_term_list

chroma_latex_labels = ['$Q_x$',
                '$Q_y$',
                '$\\frac{\partial Q_x}{\partial p}$',
                '$\\frac{\partial^2 Q_x}{\partial p^2}$',
                '$\\frac{\partial^3 Q_x}{\partial p^3}$',
                '$\\frac{\partial^4 Q_x}{\partial p^4}$',
                '$\\frac{\partial^5 Q_x}{\partial p^5}$',
                '$\\frac{\partial Q_y}{\partial p}$',
                '$\\frac{\partial^2 Q_y}{\partial p^2}$',
                '$\\frac{\partial^3 Q_y}{\partial p^3}$',
                '$\\frac{\partial^4 Q_y}{\partial p^4}$',
                '$\\frac{\partial^5 Q_y}{\partial p^5}$'
                ]
anh_latex_labels = [
        '$\\frac{\partial Q_x}{\partial J_x}$',
        '$\\frac{\partial Q_x}{\partial J_y}$',
        '$\\frac{\partial^2 Q_x}{\partial J_x \partial J_y}$',
        '$\\frac{\partial^2 Q_x}{\partial J_x \partial p}$',
        '$\\frac{\partial^2 Q_x}{\partial J_y \partial p}$',
        '$\\frac{\partial^3 Q_x}{\partial J_x \partial J_y \partial p}$',
        '$\\frac{\partial^2 Q_x}{\partial J^{2}_x}$',
        '$\\frac{\partial^2 Q_x}{\partial J^{2}_y}$',
        '$\\frac{\partial Q_y}{\partial J_x}$',
        '$\\frac{\partial Q_y}{\partial J_y}$',
        '$\\frac{\partial^2 Q_y}{\partial J_x \partial J_y}$',
        '$\\frac{\partial^2 Q_y}{\partial J_x \partial p}$',
        '$\\frac{\partial^2 Q_y}{\partial J_y \partial p}$',
        '$\\frac{\partial^3 Q_x}{\partial J_x \partial J_y \partial p}$',
        '$\\frac{\partial^2 Q_y}{\partial J^{2}_x}$',
        '$\\frac{\partial^2 Q_y}{\partial J^{2}_y}$',
        ]

PLOTCHROM = 1
PLOTANH   = 1

files_to_compare=['../Outputdata/ptc/V23_z_results.txt', '../Outputdata/ptc_rad/V23_z_results.txt']

#right now it opens each file for each set of orders, would be better to open each file only one time and then store all the values in python to continue on, less file reading.
if PLOTCHROM == 1:
    for name in tune_names:
        print("------------------------\n"+name)
        i=0
        x_chroma_results= np.empty((2,len(orders)))
        y_chroma_results= np.empty((2,len(orders)))

        for file in files_to_compare:
            df = tfs.read(file)
            for order in orders:
                x_chroma_results[i][order-1] = (1/math.factorial(order)) * df[(df["NAME"] == "DQ1") & (df["ORDER1"] == order)]["VALUE"]
                y_chroma_results[i][order-1] = (1/math.factorial(order)) * df[(df["NAME"] == "DQ2") & (df["ORDER1"] == order)]["VALUE"]
        
            print("Printing for")
            print(i)
            i=i+1
            
    terms = chroma_latex_labels[2:]
    values = np.concatenate((x_chroma_results,y_chroma_results),axis=1)
    xaxis = np.arange(len(chroma_latex_labels[2:]))
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif',weight=6,size=25)
    plt.figure(figsize=(12,9))
    plt.bar(xaxis-0.2,abs(values[0][:]),label="No Radiation",width=0.4) #MAKE SURE THAT LATTICE lines up with file list, perhaps use file_list directly
    plt.bar(xaxis+0.2,abs(values[1][:]),label="Radiation",width=0.4) #MAKE SURE THAT LATTICE lines up with file list, perhaps use file_list directly
    plt.yscale('log')
    plt.legend()
    plt.title("V23 Chromaticity with Radiation")
    plt.ylabel("Chromaticity")
    plt.xticks(xaxis,chroma_latex_labels[2:],rotation=0)
    plt.xlabel("Term")
    plt.savefig("../Outputdata/figures/ptc_rad/v23z_rad_chroma.png")
    plt.close()

if PLOTANH == 1:
    for name in tune_names:
        print("------------------------\n"+name)
        i=0
        x_anh_results= np.empty((2,len(anh_combinations)))
        y_anh_results= np.empty((2,len(anh_combinations)))

        for file in files_to_compare:
            df = tfs.read(file)
            for order in anh_combinations:
                x_anh_results[i][anh_combinations.index(order)] = df[(df["NAME"] == "ANHX") & (df["ORDER1"]==order[0]) & (df["ORDER2"]==order[1]) & (df["ORDER3"]==order[2])]["VALUE"]
                y_anh_results[i][anh_combinations.index(order)] = df[(df["NAME"] == "ANHY") & (df["ORDER1"]==order[0]) & (df["ORDER2"]==order[1]) & (df["ORDER3"]==order[2])]["VALUE"]
            
            print("Printing for")
            print(i)
            i=i+1

    terms = anh_latex_labels
    values = np.concatenate((x_anh_results,y_anh_results),axis=1)
    xaxis = np.arange(len(anh_latex_labels))
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif',weight=6,size=25)
    plt.figure(figsize=(18,9))
    plt.bar(xaxis-0.2,abs(values[0][:]),label="No Radiation",width=0.4) #MAKE SURE THAT LATTICE lines up with file list, perhaps use file_list directly
    plt.bar(xaxis+0.2,abs(values[1][:]),label="Radiation",width=0.4) #MAKE SURE THAT LATTICE lines up with file list, perhaps use file_list directly
    plt.yscale('log')
    plt.title("V23 Anharmonicity with Radiation")
    plt.ylabel("Anharmonicity")
    plt.xticks(xaxis,anh_latex_labels,rotation=0)
    plt.xlabel("Term")
    plt.legend()
    plt.tight_layout()
    plt.savefig("../Outputdata/figures/ptc_rad/v23z_rad_anh.png")
    plt.close()

#if PLOTCHROM == 1:
#    for term in chroma_terms:
#        print("------------------------\n"+term)
#        order = chroma_order[chroma_terms.index(term)]
#        values = np.empty(len(file_list))
#
#        for file in file_list:
#            df = pd.read_csv(ngmad_result_folder+file,sep="\t",index_col=0,names=["Term","Value"])
#            values[file_list.index(file)] = (1/math.factorial(order)) * df.loc[term].Value
#        print(values)
#        plt.bar(LATTICE,abs(values),width=0.5) #MAKE SURE THAT LATTICE lines up with file list, perhaps use file_list directly
#        plt.yscale('log')
#        plt.title(term)
#        plt.ylabel(str(order)+"-order chromaticity")
#        plt.xticks(rotation=45)
#        plt.xlabel("Lattice")
#        plt.tight_layout()
#        plt.savefig("../Outputdata/figures/ngmad/"+term+"_chroma.png")
#        plt.close()
#
#if PLOTANH ==1:
#    for term in anh_terms:
#        print("------------------------\n"+term)
#        values = np.empty(len(file_list))
#
#        for file in file_list:
#            df = pd.read_csv(ngmad_result_folder+file,sep="\t",index_col=0,names=["Term","Value"])
#            values[file_list.index(file)] = df.loc[term].Value
#        print(values)
#        plt.bar(LATTICE,abs(values),width=0.5)
#        plt.yscale('log')
#        plt.title(term)
#        plt.ylabel(term+" anharmonicity")
#        plt.xticks(rotation=45)
#        plt.xlabel("Lattice")
#        plt.tight_layout()
#        plt.savefig("../Outputdata/figures/ngmad/"+term+"_anharmon.png")
#        plt.close()
