import matplotlib.pyplot as plt
import numpy as np
import tfs
from cpymad.madx import Madx
import pandas as pd
import re
from matplotlib.ticker import FormatStrFormatter
import os


fontsize = 25
def plot_twiss_chroma(frame,df,Qplane,file):
	if Qplane == "Q1":
		frame.plot(df.s,df.betx,linestyle="-",label=file.rstrip("_twiss.csv"))#correct so it is x or y as required
		frame.set_ylabel("Beta X",fontsize=fontsize)
	elif Qplane == "Q2":
		frame.plot(df.s,df.bety,linestyle="-",label=file.rstrip("_twiss.csv"))#correct so it is x or y as required
		frame.set_ylabel("Beta Y",fontsize=fontsize)
	else:
		print("Invalid Qplane, must be Q1 or Q2")
	frame.set_xlabel("s", fontsize=fontsize)
	frame.set_xlim([21500,24000])#area around an IP
	frame.legend(ncol=2,fancybox=True,shadow=True)
	return frame


twiss_list = [
#'dp_-0.001_twiss.csv',
#'dp_-0.002_twiss.csv',
#'dp_-0.003_twiss.csv',
#'dp_-0.004_twiss.csv',
#'dp_-0.005_twiss.csv',
#'dp_-0.006_twiss.csv',
#'dp_-0.007_twiss.csv',
#'dp_-0.008_twiss.csv',
#'dp_-0.009_twiss.csv',
'dp_-0.01_twiss.csv',
#'dp_0.001_twiss.csv',
#'dp_0.002_twiss.csv',
#'dp_0.003_twiss.csv',
#'dp_0.004_twiss.csv',
#'dp_0.005_twiss.csv',
#'dp_0.006_twiss.csv',
#'dp_0.007_twiss.csv',
#'dp_0.008_twiss.csv',
#'dp_0.009_twiss.csv',
'dp_0.01_twiss.csv',
'dp_0.0_twiss.csv',
]

fig,ax = plt.subplots(nrows=2,ncols=4,figsize=(36,18))
twiss_folder_dict = {
	'../Outputdata/twiss_HFD_t/' : {'framex' : ax[0,0],'framey' : ax[0,1]},
	'../Outputdata/twiss_HFD_z/' : {'framex' : ax[0,2],'framey' : ax[0,3]},
	'../Outputdata/twiss_V22_t/' : {'framex' : ax[1,0],'framey' : ax[1,1]},
	'../Outputdata/twiss_V22_z/' : {'framex' : ax[1,2],'framey' : ax[1,3]}
}

for folder in twiss_folder_dict.keys():
	framex = twiss_folder_dict[folder]['framex']
	framey = twiss_folder_dict[folder]['framey']
	framex.title.set_text(folder.lstrip("../Outputdata/twiss_").rstrip("/")+" Beta x")
	framey.title.set_text(folder.lstrip("../Outputdata/twiss_").rstrip("/")+" Beta y")
	#for file in os.listdir(folder):
	for file in twiss_list:
		f = os.path.join(folder,file)
		if os.path.isfile(f):
			df = pd.read_csv(f, sep='\t', header=0,index_col=0)
			framex = plot_twiss_chroma(framex,df,"Q1",file)
			framey = plot_twiss_chroma(framey,df,"Q2",file)

fig.tight_layout()
plt.savefig("../Outputdata/figures/chroma_twiss.png")
