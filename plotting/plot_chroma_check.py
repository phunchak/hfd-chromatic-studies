import matplotlib.pyplot as plt
import numpy as np
import tfs
from cpymad.madx import Madx
import pandas as pd
import re
from matplotlib.ticker import FormatStrFormatter



def plot_tune(frame,df,Qplane,file):
	frame.plot(df.index,df[Qplane].to_list(),marker="o",linestyle="-",label=Qplane+": "+file.lstrip('Outputdata/').rstrip('_df_chroma.csv'))
	frame.axvline(x=0,linestyle="--",zorder=0,color='black')
	frame.yaxis.set_major_formatter(FormatStrFormatter('%.3f'))
	frame.set_xlabel("dp")
	frame.set_ylabel("Tune "+Qplane)
	frame.legend()
	return frame

def plot_tune_shift(frame,df,Qplane,file):
	frame.plot(df.index,df[Qplane].to_list()-(np.ones(len(df[Qplane]))*df.loc[0.0][Qplane]),marker="o",linestyle="-",label=Qplane+": "+file.lstrip('Outputdata/').rstrip('_df_chroma.csv'))
	frame.axvline(x=0,linestyle="--",zorder=0,color='black')
	frame.yaxis.set_major_formatter(FormatStrFormatter('%.3f'))
	frame.set_xlabel("dp")
	frame.set_ylabel("Tune Shift delta_"+Qplane)
	frame.legend()
	return frame

PLOTTUNE 	= 0
PLOTTUNESHIFT 	= 1


if PLOTTUNE ==1:
	fig1,ax1 = plt.subplots(nrows=4,ncols=2,figsize=(18,9))
	chroma_tune_shift_dict = {
		'../Outputdata/HFD_t_df_chroma.csv' : {'framex' : ax1[0,0],'framey' : ax1[0,1]},
		'../Outputdata/HFD_z_df_chroma.csv' : {'framex' : ax1[1,0],'framey' : ax1[1,1]},
		'../Outputdata/V22_t_df_chroma.csv' : {'framex' : ax1[2,0],'framey' : ax1[2,1]},
		'../Outputdata/V22_z_df_chroma.csv' : {'framex' : ax1[3,0],'framey' : ax1[3,1]}
	}
	
	for file in chroma_tune_shift_dict.keys():
		framex = chroma_tune_shift_dict[file]['framex']
		framey = chroma_tune_shift_dict[file]['framey']
		df = pd.read_csv(file, sep='\t', header=0,index_col=0)
		framex = plot_tune(framex,df,"Q1",file)
		framey = plot_tune(framey,df,"Q2",file)
	
	fig1.tight_layout()
	
	plt.savefig("../Outputdata/figures/tune_vs_delta_p.png")

if PLOTTUNESHIFT ==1:
	fig2,ax2 = plt.subplots(nrows=1,ncols=2,figsize=(18,9))
	chroma_tune_shift_dict = {
		'../Outputdata/HFD_t_df_chroma.csv' : {'framex' : ax2[0],'framey' : ax2[1]},
		'../Outputdata/HFD_z_df_chroma.csv' : {'framex' : ax2[0],'framey' : ax2[1]},
		'../Outputdata/V22_t_df_chroma.csv' : {'framex' : ax2[0],'framey' : ax2[1]},
		'../Outputdata/V22_z_df_chroma.csv' : {'framex' : ax2[0],'framey' : ax2[1]}
	}
	
	for file in chroma_tune_shift_dict.keys():
		framex = chroma_tune_shift_dict[file]['framex']
		framey = chroma_tune_shift_dict[file]['framey']
		df = pd.read_csv(file, sep='\t', header=0,index_col=0)
		framex = plot_tune_shift(framex,df,"Q1",file)
		framey = plot_tune_shift(framey,df,"Q2",file)
	
	fig2.tight_layout()
	
	plt.savefig("../Outputdata/figures/tune_shift_vs_delta_p.png")
