import matplotlib.pyplot as plt
import numpy as np
import tfs
from cpymad.madx import Madx
import pandas as pd
import re
import math
from matplotlib.ticker import FormatStrFormatter
import os
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import json
import itertools
#load lattice parameters
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))
LATTICE = sorted([version+mode for version in REF_FILE['VERSION'].keys() for mode in REF_FILE['OPERATION_MODES'].keys()])

ptc_result_folder = "../Outputdata/ptc/"
file_list = sorted(os.listdir(ptc_result_folder))
print(file_list)
print(LATTICE)

order_col = ["ORDER1","ORDER2","ORDER3"]
orders = [1,2,3,4,5]
anh_combinations = [[1,0,0],[0,1,0],[1,1,0],[1,0,1],[0,1,1],[1,1,1],[2,0,0],[0,2,0]]

tune_names = ["DQ1","DQ2"]
anh_names  = ["ANHX","ANHY"]

xchroma_term_list   = ['q1_ref','dq1_dp1','dq1_dp2','dq1_dp3','dq1_dp4','dq1_dp5']
ychroma_term_list   = ['q2_ref','dq2_dp1','dq2_dp2','dq2_dp3','dq2_dp4','dq2_dp5']
chroma_terms        = xchroma_term_list+ychroma_term_list
chroma_order        = list(range(0,6,1)) + list(range(0,6,1))

xanh_term_list  = ['dq1_djx1','dq1_djy1','dq1_djx1dp1','dq1_djx1djy1','dq1_djy1dp1','dq1_djx2','dq1_djy2']
yanh_term_list  = ['dq2_djx1','dq2_djy1','dq2_djx1dp1','dq2_djx1djy1','dq2_djy1dp1','dq2_djx2','dq2_djy2']
anh_terms       = xanh_term_list+yanh_term_list

PLOTCHROM = 1
PLOTANH   = 1

#right now it opens each file for each set of orders, would be better to open each file only one time and then store all the values in python to continue on, less file reading.


if PLOTCHROM == 1:
    for name in tune_names:
        print("------------------------\n"+name)
        for order in orders:
            print(str(order)+"-order term")
            values = np.empty(len(file_list))
            for file in file_list:
                df = tfs.read(ptc_result_folder+file)
                values[file_list.index(file)] = (1/math.factorial(order)) * df[(df["NAME"] == name) & (df["ORDER1"] == order)]["VALUE"]

            plt.bar(LATTICE,abs(values),width=0.5)
            plt.yscale('log')
            plt.title(name+" "+str(order)+"-order term")
            plt.ylabel(str(order)+"-order chromaticity")
            plt.xticks(rotation=45)
            plt.xlabel("Lattice")
            plt.savefig("../Outputdata/figures/ptc/order_"+str(order)+"_"+name+"_chroma.png")
            plt.close()

if PLOTANH ==1:
    for name in anh_names:
        print("------------------------\n"+name)
        for orders in anh_combinations:
            values = np.empty(len(file_list))			
            print(str(orders)+"-order term")
            for file in file_list:
                df = tfs.read(ptc_result_folder+file) 
                values[file_list.index(file)] = df[(df["NAME"] == name) & (df["ORDER1"]==orders[0]) & (df["ORDER2"]==orders[1]) & (df["ORDER3"]==orders[2])]["VALUE"]

            plt.bar(LATTICE,abs(values),width=0.5)
            plt.yscale('log')
            plt.title(name+" "+str(orders)+"-order term")
            plt.ylabel(str(orders)+"-order anharmonicty")
            plt.xticks(rotation=45)
            plt.xlabel("Lattice")
            plt.savefig("../Outputdata/figures/ptc/order_"+str(orders)+"_"+name+".png")
            plt.close()
