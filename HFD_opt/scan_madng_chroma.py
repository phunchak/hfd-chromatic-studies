import matplotlib.pyplot as plt
import numpy as np
import tfs
from cpymad.madx import Madx
import pandas as pd
import re
import os
import json
import itertools
#load lattice parameters
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))


def madng_lattice_chroma_check(sequence,mode):
    energy = str(REF_FILE[mode]["ENERGY"])
    emitX  = str(REF_FILE[mode]["EMIT_X"])
    emitY  = str(REF_FILE[mode]["EMIT_Y"])
    os.system("/afs/cern.ch/user/m/mad/madng/releases/0.9/mad-linux-0.9.7 madng_chroma.mad ../lattices/"+sequence+"_lattices/fccee_"+mode+".conv.seq "+energy+" "+emitX+" "+emitY+" "+sequence+mode)
     
# scan list
if not os.path.exists('../Outputdata/ngmad/'):
    os.makedirs('../Outputdata/ngmad/')

#for (sequence,mode) in SEQUENCES:
#    madng_lattice_chroma_check(sequence,mode)
madng_lattice_chroma_check('V23','z')

