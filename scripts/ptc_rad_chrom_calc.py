import matplotlib.pyplot as plt
import numpy as np
import tfs
from cpymad.madx import Madx
import pandas as pd
import re
import os
import json
import itertools
#load lattice parameters
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))


def ptc_lattice_chroma_check(sequence,mode):
    with Madx() as mad:
        mad.call(REF_FILE["VERSION"][sequence]["DIRECTORY"]+REF_FILE["OPERATION_MODES"][mode])
        #mad.set(FORMAT="19.15f")
        mad.beam(pc=REF_FILE[mode]['ENERGY'])
        mad.use("FCCEE_P_RING")
        mad.input('''VOLTCA1SAVE = VOLTCA1; VOLTCA2SAVE = VOLTCA2;''')
        mad.input('''VOLTCA1 = 0;VOLTCA2 = 0;RF_ON = 0;''') #first 2 settings for v22, last for hfd
        mad.seqedit(sequence="FCCEE_P_RING")
        mad.flatten()
        mad.cycle(start="ip.1")
        mad.flatten()
        mad.endedit()
        mad.use("FCCEE_P_RING")
        mad.input("SAVEBETA, LABEL=B.IP, PLACE=ip.1, SEQUENCE=FCCEE_P_RING;")
        mad.twiss(file = "chrom_twiss_no_taper.tfs",tolerance=1e-12, chrom=True) #twiss without radiation/tapering
        mad.input('''betxip = table(twiss, ip.1, betx);
                    betyip = table(twiss, ip.1, bety);
                    alfxip = table(twiss, ip.1, alfx);
                    alfyip = table(twiss, ip.1, alfy);

                    VALUE, betxip;
                    VALUE, betyip;
                    VALUE, alfxip;
                    VALUE, alfyip;''')
        mad.use("FCCEE_P_RING")
        #turn back on cavities
        mad.input('''VOLTCA1 = VOLTCA1SAVE; VOLTCA2 = VOLTCA2SAVE;''')
        mad.beam(radiate=True) #radiation on
        mad.input('''VALUE, lagca1; VALUE, lagca2;''')
        mad.input('''LAGCA1 = 0.5;''')
        mad.input('''MATCH, sequence=FCCEE_P_RING, BETA0 = B.IP, tapering;
                    VARY, NAME=LAGCA1, step=1.0E-7;
                    CONSTRAINT, SEQUENCE=FCCEE_P_RING, RANGE=#e, PT=0.0;
                    JACOBIAN,TOLERANCE=1.0E-14, CALLS=3000;
                    ENDMATCH; ''')
        mad.use("FCCEE_P_RING")
        mad.twiss(file = "chrom_twiss_tapered.tfs",tolerance=1e-6,tapering=True,chrom=True)

        if not os.path.exists('../Outputdata/ptc_rad/'):
           os.makedirs('../Outputdata/ptc_rad/')
        #mad.use("FCCEE_P_RING")
        mad.call(file='/afs/cern.ch/user/p/phunchak/work/public/hfd-chromatic-studies/scripts/macro_ptc_chroma.madx')
        mad.input("EXEC, ptc_data;")
        mad.input("Write, TABLE=normal_results, file='../Outputdata/ptc_rad/"+sequence+"_"+mode+"_results.txt';")


#for (sequence, mode) in SEQUENCES:
#    ptc_lattice_chroma_check(sequence,mode)
ptc_lattice_chroma_check('V23','z')

