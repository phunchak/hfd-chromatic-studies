import matplotlib.pyplot as plt
import numpy as np
import tfs
from cpymad.madx import Madx
import pandas as pd
import re
import os
import json
import itertools

#load lattice parameters
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))


delta_p_max = 0.01 #need a way to 'catch' twiss failed and continue
delta_p_steps = 51 #should be odd to include dp=0
delta_p = np.linspace(-delta_p_max,delta_p_max,delta_p_steps)

df_columns = ['Q1','Q2','dQ1','dQ2','DX','DY','Beta_x*','Beta_y*','Alpha_x*','Alpha_y*']

def lattice_chroma_check(delta_p,sequence,mode):
	with Madx() as mad:
		mad.call(REF_FILE["VERSION"][sequence]["DIRECTORY"]+REF_FILE["OPERATION_MODES"][mode])
		mad.beam(pc=REF_FILE[mode]['ENERGY']) 
		mad.use("FCCEE_P_RING") 
		mad.input('''VOLTCA1 = 0;VOLTCA2 = 0;RF_ON = 0;''') #first two settings are for v22, last is for hfd
		sequence_df = pd.DataFrame(index=delta_p,columns=df_columns)
		if not os.path.exists('../Outputdata/twiss_'+sequence+"_"+mode+'/'):
			os.makedirs('../Outputdata/twiss_'+sequence+"_"+mode+'/')
		mad.input("SELECT, flag=twiss, column=name,keyword,s,betx,alfx,mux,bety,alfy,muy,x,px,y,py,t,pt,dx,dpx,dy,dpy,wx,phix,dmux,wy,phiy,dmuy,ddx,ddpx,ddy,ddpy,n1") 
		for dp in delta_p:
			print("--------\n")
			print("Beginning Twiss run for "+sequence+"_"+mode)
			print("with delta_p = {:.3f}".format(dp))
			print("--------\n")
			twiss_df = mad.twiss(chrom=True,deltap="{:.5f}".format(dp)).dframe()
			twiss_df.set_index(twiss_df.columns[0])
			sequence_df.loc[dp]=[
					str(mad.table.summ["Q1"]).strip("[]"),
					str(mad.table.summ["Q2"]).strip("[]"),
					str(mad.table.summ["dQ1"]).strip("[]"),
					str(mad.table.summ["dQ2"]).strip("[]"),
					twiss_df.loc[REF_FILE["VERSION"][sequence]["IP_NAME"]]["dx"],
					twiss_df.loc[REF_FILE["VERSION"][sequence]["IP_NAME"]]["dy"],
					twiss_df.loc[REF_FILE["VERSION"][sequence]["IP_NAME"]]["betx"],
					twiss_df.loc[REF_FILE["VERSION"][sequence]["IP_NAME"]]["bety"],
					twiss_df.loc[REF_FILE["VERSION"][sequence]["IP_NAME"]]["alfx"],
					twiss_df.loc[REF_FILE["VERSION"][sequence]["IP_NAME"]]["alfy"]
			]
			twiss_df.to_csv("../Outputdata/twiss_"+sequence+"_"+mode+'/'+"dp_"+"{:.3f}".format(dp)+"_twiss.csv",sep="\t") 
		
		sequence_df.to_csv("../Outputdata/"+sequence+"_"+mode+"_df_chroma.csv",sep='\t')
	return 

for (sequence, mode) in SEQUENCES:
    lattice_chroma_check(delta_p,sequence,mode)


