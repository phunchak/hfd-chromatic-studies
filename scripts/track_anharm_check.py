import matplotlib.pyplot as plt
import numpy as np
import tfs
from cpymad.madx import Madx
import pandas as pd
import re
import os
import json
import itertools

#load lattice parameters
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))



def lattice_anharm_check(sequence,mode):
    with Madx() as mad:
        mad.call(REF_FILE["VERSION"][sequence]["DIRECTORY"]+REF_FILE["OPERATION_MODES"][mode])
        mad.beam(pc=REF_FILE[mode]['ENERGY'])
        mad.use("FCCEE_P_RING")
        mad.input("BEAM, RADIATE=TRUE;")
        mad.input("LAGCA1 = 0.5;")
#        mad.input('''MATCH, sequence=fccee_p_ring, BETA0 = B.IP, tapering;
#                      VARY, NAME=LAGCA1, step=1.0E-7;
#                      CONSTRAINT, SEQUENCE=fccee_p_ring, RANGE=#e, PT=0.0;
#                      JACOBIAN,TOLERANCE=1.0E-14, CALLS=9000;
#                    ENDMATCH;''')
#        mad.use("FCCEE_P_RING")
#        mad.twiss(Tapering = True, file="tapered_twiss_anharm_tracking")
#        mad.input('''VOLTCA1 = 0;VOLTCA2 = 0;RF_ON = 0;''') #first two settings are for v22, last is for hfd
        #sequence_df = pd.DataFrame(index=delta_p,columns=df_columns)
        if not os.path.exists('../Outputdata/twiss_'+sequence+"_"+mode+'/'):
            os.makedirs('../Outputdata/twiss_'+sequence+"_"+mode+'/')
        mad.input('''SELECT, FLAG=makethin, CLASS=RFCAVITY, SLICE = 1;
            SELECT, FLAG=makethin, CLASS=rbend, SLICE = 8;          !originally 4 slices
            SELECT, FLAG=makethin, CLASS=quadrupole, SLICE = 6;     !originally 4 slices
            SELECT, FLAG=makethin, CLASS=sextupole, SLICE = 6;      !originally 4 slices
    
            SELECT, FLAG=makethin, PATTERN="QC*", SLICE=20;
            SELECT, FLAG=makethin, PATTERN="SY*", SLICE=20;

            MAKETHIN, SEQUENCE=FCCEE_P_RING STYLE=TEAPOT, MAKEDIPEDGE=false;

            USE, SEQUENCE = FCCEE_P_RING;

            TWISS, FILE="twiss_FCCee_z_thin_tapered.tfs", TOLERANCE=1E-12;

            SELECT, FLAG = TABLE, COLUMN=NUMBER,TURN,X,PX,Y,PY,T,PT,S,E;''')

        mad.input('''betxstart = table(twiss, IP.1, betx); 
                betystart = table(twiss, IP.1, bety);
                alfxstart = table(twiss, IP.1, alfx);
                alfystart = table(twiss, IP.1, alfy);
                EX = 0.71E-9;
                EY = 1.9E-12;

                ''')

        mad.input("EOPTION, SEED=1;")
        mad.input("TRACK, ONETABLE=FALSE, FILE='track_anharm.tfs', DAMP=TRUE,RECLOSS=TRUE,QUANTUM=FALSE;")
        #mad.input("START, x=0.0001,px=0,y=0,py=0;")
        #mad.input("START, x=0,px=0,y=0.000001,py=0;")
        #mad.input("START, x=0.0002,px=0,y=0,py=0;")
        mad.input("START, FX=15*SQRT(EX),PHIX=0,FY=0,PHIY=0;")
        mad.input("START, FX=0,PHIX=0,FY=15*SQRT(EY),PHIY=0;")
        mad.input("START, FX=15*SQRT(EX),PHIX=0,FY=15*SQRT(EY),PHIY=0;")
        mad.input("OBSERVE, PLACE=QD3.60;")
        mad.input("RUN, TURNS=5000;")
        mad.input("ENDTRACK;")
        mad.input("VALUE, betxstart;")

lattice_anharm_check("V23","z")
