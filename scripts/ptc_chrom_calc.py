import matplotlib.pyplot as plt
import numpy as np
import tfs
from cpymad.madx import Madx
import pandas as pd
import re
import os
import json
import itertools
#load lattice parameters
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))


def ptc_lattice_chroma_check(sequence,mode):
    with Madx() as mad:
        mad.call(REF_FILE["VERSION"][sequence]["DIRECTORY"]+REF_FILE["OPERATION_MODES"][mode])
        #mad.set(FORMAT="19.15f")
        mad.beam(pc=REF_FILE[mode]['ENERGY'])
        mad.use("FCCEE_P_RING")
        mad.input('''VOLTCA1 = 0;VOLTCA2 = 0;RF_ON = 0;''') #first 2 settings for v22, last for hfd
        if not os.path.exists('../Outputdata/ptc/'):
            os.makedirs('../Outputdata/ptc/')
        mad.call(file='/afs/cern.ch/user/p/phunchak/work/public/hfd-chromatic-studies/scripts/macro_ptc_chroma.madx')
        mad.input("EXEC, ptc_data;")
        mad.input("Write, TABLE=normal_results, file='../Outputdata/ptc/"+sequence+"_"+mode+"_results.txt';")


for (sequence, mode) in SEQUENCES:
    ptc_lattice_chroma_check(sequence,mode)

