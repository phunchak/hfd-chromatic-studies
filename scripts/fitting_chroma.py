import matplotlib.pyplot as plt
import numpy as np
import tfs
from cpymad.madx import Madx
import pandas as pd
import re
from matplotlib.ticker import FormatStrFormatter

PLOT_FIT 	= 1
FIT_ORDER 	= 20

chroma_tune_shift_dict = {
    '../Outputdata/HFD_61_t_df_chroma.csv':	        'HFD61_t',
    '../Outputdata/HFD_61_z_df_chroma.csv':	        'HFD61_z',
    '../Outputdata/V22_t_df_chroma.csv':	        'V22_t',
    '../Outputdata/V22_z_df_chroma.csv':	        'V22_z',
    '../Outputdata/HFD_79_t_df_chroma.csv':	        'HFD79_t',
    '../Outputdata/HFD_79_z_df_chroma.csv':	        'HFD79_z',
    '../Outputdata/V23_t_df_chroma.csv':	        'V23_t',
    '../Outputdata/V23_z_df_chroma.csv':	        'V23_z',
    '../Outputdata/V23_zrad_tapered_df_chroma.csv':	'V23_tap_z',
    '../Outputdata/HFD_79_zrad_tapered_df_chroma.csv':	'HFD79_z_tap_z'
}
#chroma_tune_shift_dict = {
#   '../Outputdata/V23_zrad_tapered_df_chroma.csv':	'V23_tap_z'
#}

#make dataframe with chroma coeff
df_higher_chroma = pd.DataFrame( columns = chroma_tune_shift_dict.values(), index = ['d1Qx','d2Qx','d3Qx','d4Qx','d5Qx','d1Qy','d2Qy','d3Qy','d4Qy','d5Qy'])

for key in chroma_tune_shift_dict.keys():
    df = pd.read_csv(key, delimiter='\t', header=0, index_col=0)

    deltap 	= df.index.to_list()
    Q1 	= df.Q1.to_list()
    Q2 	= df.Q2.to_list()

    pQ1 = np.polyfit(deltap,Q1,deg=FIT_ORDER)
    pQ2 = np.polyfit(deltap,Q2,deg=FIT_ORDER)
    pxy = np.concatenate((np.flip(pQ1[15:-1]),np.flip(pQ2[15:-1])))
    df_higher_chroma[chroma_tune_shift_dict[key]] = pxy

    if PLOT_FIT == 1:
        fig,ax = plt.subplots(nrows=1,ncols=2,figsize=(18,9))

        ax[0].plot(deltap,Q1, marker= 'o', linestyle='--')
        ax[0].plot(deltap,np.polyval(pQ1,deltap),linestyle='-',marker= 'x')
        ax[1].plot(deltap,Q2, marker= 'o', linestyle='--')
        ax[1].plot(deltap,np.polyval(pQ2,deltap),linestyle='-',marker= 'x')
        ax[0].set_ylabel("Q1")
        ax[1].set_ylabel("Q2")
        plt.savefig('../Outputdata/figures/'+chroma_tune_shift_dict.get(key)+'_fitting.png')
    
        
df_higher_chroma.to_csv("../Outputdata/fitting_chroma/high_order_chroma.tfs", sep='\t')

