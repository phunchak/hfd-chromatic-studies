# content of test_sample.py
import pytest
import json
import pandas as pd
#from pandas._testing import assert_frame_equal, assert_series_equal
import tfs
import numpy as np
import itertools
import xobjects as xo
import xtrack as xt
import xpart as xp
import xfields as xf


def func(sequence,mode):
    with open('../lattices/lattice_parameters.json', 'r') as file:
       REF_FILE = json.load(file)
    #make list of lattices
    SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))
    line = xt.Line.from_json('lattices_xsuite/'+sequence+mode+'_lattice.json')
    
    #positron for z lattice
    energy = REF_FILE["VERSION"][sequence][mode]["ENERGY"]*1e9
    line.particle_ref = xt.Particles(mass0=xt.ELECTRON_MASS_EV, q0=1, energy0=energy)
    line.build_tracker()
    # We consider a case in which all RF cavities are off
    tab = line.get_table()
    tab_cav = tab.rows[tab.element_type == 'Cavity']
    for nn in tab_cav.name:
        line[nn].voltage = 0
    
    tw = line.twiss(method='4d', delta0=0) #Twiss with RF of
    return  tw.qx

def test_answer():
    assert abs(func("V24.3_GHC","z") - 218.158433868465522) < 0.001
