import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import xfields as xf
from cpymad.madx import Madx
import matplotlib.pyplot as plt
import pandas as pd
import tfs
from scipy.constants import e as qe
from scipy.constants import m_p
import math
import itertools

#CURRENTLY HARDCODED FOR HIBS GHC z
#load lattice parameters
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))

sequence = "V24.3_GHC"
mode = "z"

print("Translating MAD-X Sequence to xsuite format")

#Load Sequence from MADX .seq file
with open('madx_output.log', 'w') as f:
    mad = Madx(stdout=f)
    mad.input("SET, FORMAT = '19.15f';")
    mad.input("CALL, FILE = '/afs/cern.ch/user/p/phunchak/work/public/hfd-chromatic-studies/lattices/V24.3_GHC_lattices/fccee_z.seq';")
    mad.input("CALL, FILE = '/afs/cern.ch/user/p/phunchak/work/public/hfd-chromatic-studies/lattices/"+sequence+"_lattices/install_apertures.madx';") 
    mad.input("pbeam  = "+str(REF_FILE["VERSION"][sequence][mode]["ENERGY"])+";")
    mad.input("EXbeam = "+str(REF_FILE["VERSION"][sequence][mode]["EMIT_X"])+";")
    mad.input("EYbeam = "+str(REF_FILE["VERSION"][sequence][mode]["EMIT_Y"])+";")
    mad.input("""
            Ebeam := sqrt( pbeam^2 + emass^2 ); !energy of the beam

            // Beam defined without radiation as a start - radiation is turned on later depending on the requirements
            BEAM, PARTICLE=POSITRON, NPART=1.7e11, KBUNCH=16640, ENERGY=Ebeam, RADIATE=FALSE, BV=+1, EX=EXbeam, EY=EYbeam;

            USE, SEQUENCE = FCCEE_P_RING;

            """)
    mad.input("VALUE, BpipeRadiusArcs;")

    if sequence == "V24.3_LCC":
        mad.input("""CALL, FILE = 'install_matching_markers.madx'""")


#build line
line = xt.Line.from_madx_sequence(mad.sequence['FCCEE_P_RING'], apply_madx_errors=False, install_apertures=False, allow_thick=True, deferred_expressions=True)

print(line.vars['bpiperadiusarcs']._get_value())

aper_arc_beampipe = xt.LimitEllipse(a=line.vars['bpiperadiusarcs']._get_value(), b=line.vars['bpiperadiusarcs']._get_value())

tab = line.get_table()

for el in tab.rows['fd0a.2':'fd0a.3'].name:
    line.insert_element('aper', aper_arc_beampipe, index=el)

# save to json
with open('lattices_xsuite/'+sequence+mode+'_lattice.json', 'w') as fid:
    json.dump(line.to_dict(), fid, cls=xo.JEncoder,indent = 6)

# load from json
#with open('lattices_xsuite/'+sequence+mode+'_lattice.json', 'r') as fid:
#   loaded_dct = json.load(fid)

#line_2 = xt.Line.from_dict(loaded_dct)
