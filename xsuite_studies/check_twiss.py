import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import xfields as xf
from cpymad.madx import Madx
import matplotlib.pyplot as plt
import pandas as pd
import tfs
from scipy.constants import e as qe
from scipy.constants import m_p
import math
import itertools

#load lattice parameters
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))

def load_lattice(sequence,mode):
    print('---------------------------')
    print("Running twiss for:")
    print(sequence+mode)
    # load from json
    with open('lattices_xsuite/'+sequence+mode+'_lattice.json', 'r') as fid:
        loaded_dct = json.load(fid)
    
    line = xt.Line.from_dict(loaded_dct)
    
    #positron for z lattice
    energy = REF_FILE[mode]["ENERGY"]*1e9
    print(energy)
    line.particle_ref = xt.Particles(mass0=xt.ELECTRON_MASS_EV, q0=1, energy0=REF_FILE[mode]["ENERGY"]*1e9)
    line.build_tracker()
    # We consider a case in which all RF cavities are off
    tab = line.get_table()
    #tab.show()
    tab_cav = tab.rows[tab.element_type == 'Cavity']
    for nn in tab_cav.name:
        line[nn].voltage = 0
    print('---------------------------')
    tw = line.twiss(method='4d') #Twiss with RF off
    return tw
   
def plot_twiss(tw):
    # Inspect tunes and chromaticities
    tw.qx # Horizontal tune
    tw.qy # Vertical tune
    tw.dqx # Horizontal chromaticity
    tw.dqy # Vertical chromaticity
    
    # Plot closed orbit and lattice functions
    plt.close('all')
    
    fig1 = plt.figure(1, figsize=(6.4, 4.8*1.5))
    spbet = plt.subplot(3,1,1)
    spco = plt.subplot(3,1,2, sharex=spbet)
    spdisp = plt.subplot(3,1,3, sharex=spbet)
    
    spbet.plot(tw.s, tw.betx)
    spbet.plot(tw.s, tw.bety)
    spbet.set_ylabel(r'$\beta_{x,y}$ [m]')
    
    spco.plot(tw.s, tw.x)
    spco.plot(tw.s, tw.y)
    spco.set_ylabel(r'(Closed orbit)$_{x,y}$ [m]')
    
    spdisp.plot(tw.s, tw.dx)
    spdisp.plot(tw.s, tw.dy)
    spdisp.set_ylabel(r'$D_{x,y}$ [m]')
    spdisp.set_xlabel('s [m]')
    
    fig1.suptitle(
        r'$q_x$ = ' f'{tw.qx:.5f}' r' $q_y$ = ' f'{tw.qy:.5f}' '\n'
        r"$Q'_x$ = " f'{tw.dqx:.2f}' r" $Q'_y$ = " f'{tw.dqy:.2f}'
        r' $\gamma_{tr}$ = '  f'{1/np.sqrt(tw.momentum_compaction_factor):.2f}'
    )
    
    
    fig1.subplots_adjust(left=.15, right=.92, hspace=.27)
    plt.savefig('Outputdata/figures/'+sequence+'_'+mode+'_twiss.png')
    
    spbet.set_xlim([21500,24000])
    plt.savefig('Outputdata/figures/'+sequence+'_'+mode+'_twiss_IP.png')


for (sequence,mode) in SEQUENCES[0:1]:
    tw = load_lattice(sequence, mode)
    plot_twiss(tw)
