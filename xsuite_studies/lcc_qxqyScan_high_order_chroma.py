import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import xfields as xf
from cpymad.madx import Madx
import matplotlib.pyplot as plt
import pandas as pd
import tfs
from scipy.constants import e as qe
from scipy.constants import m_p
import math
import itertools
import re
import os

#load lattice parameters json
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))

def load_lattice(sequence, mode):
    # load from json
#    with open('lattices_xsuite/'+sequence+mode+'_lattice.json', 'r') as fid:
#        loaded_dct = json.load(fid)
    
#    line = xt.Line.from_dict(loaded_dct, )
    line = xt.Line.from_json('lattices_xsuite/'+sequence+mode+'_lattice.json')
    
    #positron for z lattice
    energy = REF_FILE[mode]["ENERGY"]*1e9
    line.particle_ref = xt.Particles(mass0=xt.ELECTRON_MASS_EV, q0=1, energy0=REF_FILE[mode]["ENERGY"]*1e9)
    line.build_tracker()
    # We consider a case in which all RF cavities are off
    tab = line.get_table()
    tab_cav = tab.rows[tab.element_type == 'Cavity']
    for nn in tab_cav.name:
        line[nn].voltage = 0
    
    tw = line.twiss(method='4d', delta0=0) #Twiss with RF off 
    return tw, line


def tune_chroma_match(sequence, mode,   #identity of lattice to work on
        twiss_before,                          #previously run twiss for reference tunes (no default value)
        line,                           #
        tuneMatch = 0,                  #set to 1 to run tune matching
        deltaQX = 0, deltaQY = 0,       #desired change in horiz. and vert. tunes
        chromaMatch = 0,                #set to 1 to run chromaticity matching
        deltaDQX = 0, deltaDQY = 0):    #desired change in chromaticity
    
    #make matching knobs
    
    line.vars['SF_KNOB'] = 0 #initialize sextupole delta knobs
    line.vars['SD_KNOB'] = 0 

    # apply sextupole delta knobs to the matching sextupoles
    # I don't believe this includes previous dependencies 
#    for i in range(len(sf_families)):
#        line.vars[sf_families[i]] = line.vars[sf_families[i]]._get_value() + line.vars['SF_KNOB']
#    for i in range(len(sd_families)):
#        line.vars[sd_families[i]] = line.vars[sd_families[i]]._get_value() + line.vars['SD_KNOB']
    


    # Match tunes and chromaticities to assigned values
    opt = line.match(
        solve = False, # just setting up optimization call the solve later
        start='s.arc_uu', end='e.arc_uu',
        init = twiss_before.get_twiss_init(at_element='s.arc_uu'),

        method='4d', # <- passed to twiss
        vary=[ 
            xt.Vary(name='kqd1', step=1e-6, tag='quad'),
            xt.Vary(name='kqf2', step=1e-6, tag='quad'),
            xt.Vary(name='kqd3', step=1e-6, tag='quad'),
            xt.Vary(name='kqf4', step=1e-6, tag='quad'),
            xt.Vary(name='kqd5', step=1e-6, tag='quad'),
            xt.Vary(name='kqf6', step=1e-6, tag='quad')
            # xt.VaryList(['SF_KNOB','SD_KNOB'], step=1e-4, tag='sext'),
        ],
        targets = [
            xt.Target('bety', at='qd3a:20', value=line.vars['bysd1']._get_value(), tol=1e-3),
            xt.Target('betx', at='qf4a:20', value=line.vars['bxsf1']._get_value(), tol=1e-3),
            xt.Target('bety', at='qd5a:20', value=line.vars['bysd2']._get_value(), tol=1e-3),
            xt.Target('betx', at='qf2a:20', value=line.vars['bxsf2']._get_value(), tol=1e-3),
            xt.Target('dx', at='qf6a:10', value=xt.GreaterThan(line.vars['dxmid']._get_value()*0), tol=1e-3),
            xt.Target('bety', at='e.arc_uu', value=xt.GreaterThan(line.vars['byend']._get_value()), tol=1e-3), 
            xt.Target('dx',  at='e.arc_uu', value=xt.GreaterThan(line.vars['dxend']._get_value()*0), tol=1e-3), 
            xt.Target('mux', at='e.arc_uu', value=2*line.vars['muxu']._get_value()+tw['mux','s.arc_uu']+2*deltaQX/224, tol=1e-7), #final term is tune change divided by number of unit cells, times 2 unit cells
            xt.Target('muy', at='e.arc_uu', value=2*line.vars['muyu']._get_value()+tw['muy','s.arc_uu']+2*deltaQY/224, tol=1e-7), 
        ])

    # view optimization setup
#    opt.show()
#    opt.target_status()
    # choose optimizatin targets
#    opt.disable_all_targets();
#    opt.disable_all_vary();
#    if tuneMatch == 1:
#        opt.enable_vary(tag='quad')
#        opt.enable_targets(tag='tune')
#    if chromaMatch == 1:
#        opt.enable_vary(tag='sext')
#        opt.enable_targets(tag='chroma')
   
    #solve optimization
    opt.solve()
    # Inspect optimization log
    opt.log()

    # Inspect optimization outcome
#    opt.target_status()
#    opt.vary_status()

    # Get knob values after optimization
    knobs_after_match = opt.get_knob_values()
    #print(knobs_after_match)
    # Get knob values before optimization
    knobs_before_match = opt.get_knob_values(iteration=0)
    #return line
    return knobs_after_match

def chroma_scan(line, sequence, mode, dp, deltaQX, deltaQY):
    qx_dp = np.zeros(len(dp))
    qy_dp = np.zeros(len(dp))
    print("running chroma scan")
    for i in range(len(dp)):
        try:
            tw_dp = line.twiss(method='4d', delta0 = dp[i])
            qx_dp[i] = tw_dp.qx
            qy_dp[i] = tw_dp.qy
            #print(dp[i])
            #print(tw_dp.qx)
            #print(tw_dp.qy)
        except ValueError:
            print("Twiss Failed for dp = "+str(dp[i]))
            qx_dp[i] = -1 # negative tune set to indicate failure
            qy_dp[i] = -1 # negative tune set to indicate failure
    d = {'Qx':qx_dp, 'Qy':qy_dp}
    df = pd.DataFrame(data = d, index = dp)
    if not os.path.exists('Outputdata/chromaScan/'+sequence+mode):
        os.makedirs('Outputdata/chromaScan/'+sequence+mode)
    df.to_csv('Outputdata/chromaScan/'+sequence+mode+'/qx_'+str('%.3f' % deltaQX)+'_qy_'+str('%.3f' % deltaQY)+'.txt',sep='\t')
#   print(qx_dp)
#   print(qy_dp)
    return qx_dp,qx_dp

for (sequence,mode) in SEQUENCES[0:2]:
    print("-------------------------------------------")
    print(sequence+mode+'\n\n')
    tw, line = load_lattice(sequence, mode)
    print(tw.qx)
    print(tw.qy)
    for qxdelt in np.linspace(-0.05,0.05,11):
        for qydelt in np.linspace(-0.05,0.05,11):
#    for qxdelt in [0]:
#        for qydelt in [0]:
            knobs_matched = tune_chroma_match(sequence, mode, twiss_before = tw, line = line, tuneMatch = 1, deltaQX = qxdelt, deltaQY = qydelt, chromaMatch = 0, deltaDQX = 0, deltaDQY = 0)
            #knobs_matched
            dp = np.linspace(-0.01,0.01,51)
            qx_dp, qy_dp = chroma_scan(line, sequence,mode,dp, qxdelt, qydelt)

