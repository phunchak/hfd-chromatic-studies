import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import xfields as xf
from cpymad.madx import Madx
import matplotlib.pyplot as plt
import pandas as pd
import tfs
from scipy.constants import e as qe
from scipy.constants import m_p
import math
import itertools
import re
import os

#load lattice parameters json
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))

def load_lattice(sequence, mode):
    # load from json
#    with open('lattices_xsuite/'+sequence+mode+'_lattice.json', 'r') as fid:
#        loaded_dct = json.load(fid)
    
#    line = xt.Line.from_dict(loaded_dct, )
    line = xt.Line.from_json('lattices_xsuite/'+sequence+mode+'_lattice.json')
    
    #positron for z lattice
    energy = REF_FILE["VERSION"][sequence][mode]["ENERGY"]*1e9
    line.particle_ref = xt.Particles(mass0=xt.ELECTRON_MASS_EV, q0=1, energy0=energy)
    line.build_tracker()
    # We consider a case in which all RF cavities are off
    tab = line.get_table()
    tab_cav = tab.rows[tab.element_type == 'Cavity']
    for nn in tab_cav.name:
        line[nn].voltage = 0
    
    tw = line.twiss(method='4d', delta0=0) #Twiss with RF off 
    return tw, line


def tune_chroma_match(sequence, mode,   #identity of lattice to work on
        twiss_before,                          #previously run twiss for reference tunes (no default value)
        line,                           #
        tuneMatch = 0,                  #set to 1 to run tune matching
        deltaQX = 0, deltaQY = 0,       #desired change in horiz. and vert. tunes
        chromaMatch = 0,                #set to 1 to run chromaticity matching
        deltaDQX = 0, deltaDQY = 0):    #desired change in chromaticity
    
    #make matching knobs
    sf_families = REF_FILE["VERSION"][sequence]["MATCH_SF_SEXTS"]
    sd_families = REF_FILE["VERSION"][sequence]["MATCH_SD_SEXTS"]
    
    print(sf_families)

    line.vars['SF_KNOB'] = 1 #initialize sextupole delta knobs
    line.vars['SD_KNOB'] = 1 

    # apply sextupole delta knobs to the matching sextupoles
    for i in range(len(sf_families)):
        line.vars[sf_families[i]] = line.vars[sf_families[i]]._get_value() * line.vars['SF_KNOB']
    for i in range(len(sd_families)):
        line.vars[sd_families[i]] = line.vars[sd_families[i]]._get_value() * line.vars['SD_KNOB']

    # Match tunes and chromaticities to assigned values
    opt = line.match(
        solve=False,
        method='4d',
        vary=xt.VaryList(["SF_KNOB","SD_KNOB"], step=1e-4),
        targets=xt.TargetSet(dqx=tw.dqx+deltaDQX, dqy=tw.dqx+deltaDQY, tol=1e-3)
    )
    
    # view optimization setup
#    opt.show()
#    opt.target_status()
    # choose optimizatin targets
#    opt.disable_all_targets();
#    opt.disable_all_vary();
#    if tuneMatch == 1:
#        opt.enable_vary(tag='quad')
#        opt.enable_targets(tag='tune')
#    if chromaMatch == 1:
#        opt.enable_vary(tag='sext')
#        opt.enable_targets(tag='chroma')
   
    #solve optimization
    opt.solve()
    # Inspect optimization log
    opt.log()

    # Inspect optimization outcome
#    opt.target_status()
#    opt.vary_status()

    # Get knob values after optimization
    knobs_after_match = opt.get_knob_values()
    #print(knobs_after_match)
    # Get knob values before optimization
    knobs_before_match = opt.get_knob_values(iteration=0)
    #return line
    return knobs_after_match

def chroma_scan(line, sequence, mode, dp, deltaDQX, deltaDQY):
    qx_dp = np.zeros(len(dp))
    qy_dp = np.zeros(len(dp))
    print("running chroma scan")
    for i in range(len(dp)):
        try:
            tw_dp = line.twiss(method='4d', delta0 = dp[i])
            qx_dp[i] = tw_dp.qx
            qy_dp[i] = tw_dp.qy
            #print(dp[i])
            #print(tw_dp.qx)
            #print(tw_dp.qy)
        except ValueError:
            print("Twiss Failed for dp = "+str(dp[i]))
            qx_dp[i] = -1 # negative tune set to indicate failure
            qy_dp[i] = -1 # negative tune set to indicate failure
    d = {'Qx':qx_dp, 'Qy':qy_dp}
    df = pd.DataFrame(data = d, index = dp)
    if not os.path.exists('Outputdata/chromaScan/'+sequence+mode):
        os.makedirs('Outputdata/chromaScan/'+sequence+mode)
    df.to_csv('Outputdata/chromaScan/'+sequence+mode+'/dqx_'+str('%.3f' % deltaDQX)+'_dqy_'+str('%.3f' % deltaDQY)+'.txt',sep='\t')
#   print(qx_dp)
#   print(qy_dp)
    return qx_dp,qx_dp

for (sequence,mode) in SEQUENCES[0:1]:
    print("-------------------------------------------")
    print(sequence+mode+'\n\n')
    tw, line = load_lattice(sequence, mode)
    for dqx in [0,1,2,3,4,5]:
        for dqy in [0,1,2,3,4,5]:
#   for dqx in [0]:
#       for dqy in [0]:
            knobs_matched = tune_chroma_match(sequence, mode, twiss_before = tw, line = line, tuneMatch = 0, deltaQX = 0, deltaQY = 0, chromaMatch = 1, deltaDQX = dqx, deltaDQY = dqy )
#            det_dict = line.get_amplitude_detuning_coefficients(nemitt_x = 7.5e-4, nemitt_y = 1.48e-6, num_turns = 256, a0_sigmas = 0.01, a1_sigmas = 0.1, a2_sigmas = 0.2)
            #knobs_matched
            dp = np.linspace(-0.01,0.01,51)
            qx_dp, qy_dp = chroma_scan(line, sequence,mode,dp, dqx, dqy)

