import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import xfields as xf
from cpymad.madx import Madx
import matplotlib.pyplot as plt
import pandas as pd
import tfs
from scipy.constants import e as qe
from scipy.constants import m_p
import math
import itertools
import re
import os


#load lattice parameters json
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))

def load_lattice(sequence, mode):
    # load from json
    with open('lattices_xsuite/'+sequence+mode+'_lattice.json', 'r') as fid:
        loaded_dct = json.load(fid)
    
    line = xt.Line.from_dict(loaded_dct)
    #positron for z lattice
    energy = REF_FILE["VERSION"][sequence][mode]["ENERGY"]*1e9
    line.particle_ref = xt.Particles(mass0=xt.ELECTRON_MASS_EV, q0=1, energy0=energy)
    ## Choose a context
    context = xo.ContextCpu()         # For CPU
    # context = xo.ContextCupy()      # For CUDA GPUs
    # context = xo.ContextPyopencl()  # For OpenCL GPUs
    # We consider a case in which all RF cavities are off
    line.build_tracker(_context=context)
    tab = line.get_table()
    #print(tab.rows[tab.element_type == 'Marker'])
    tab_cav = tab.rows[tab.element_type == 'Cavity']
    for nn in tab_cav.name:
        line[nn].voltage = 0
    
    tw = line.twiss(method='4d') #Twiss with RF off 

    return tw, line, tab

def install_octupole(octStr, marker, s_offset):
    my_oct = xt.Multipole(length = 0.1, knl=[0,0,0,octStr]) #KNL 
    line.discard_tracker()
    line.insert_element('corr_oct', my_oct.copy(), at_s = tab['s',marker]+s_offset)
    line.build_tracker()
    return line


def plot_twiss(tw,marker,s_offset):
    plt.close('all')

    fig1 = plt.figure(1, figsize=(6.4, 4.8*1.5))
    spbet = plt.subplot(3,1,1)
    spbet.plot(tw.s, tw.betx)
    spbet.plot(tw.s, tw.bety)
    spbet.set_ylabel(r'$\beta_{x,y}$ [m]')
    spbet.axvline(x=tab['s',marker]+s_offset)
    spbet.set_xlim([1000,5000])
    plt.show()
    return


def check_non_linear(line,df,octStr,i):
    det_dict = line.get_amplitude_detuning_coefficients(nemitt_x = nex, nemitt_y = ney, num_turns = 256, a0_sigmas = 0.01, a1_sigmas = 0.1, a2_sigmas = 0.2)
    tw = line.twiss(method='4d') #Twiss with RF off
    df.loc[i] = list(det_dict.values())+[tw.ddqx, tw.ddqy, octStr]
    return df


class ActionMeasAmplDet(xt.Action):

    def __init__(self, line, num_turns, nemitt_x, nemitt_y):

        self.line = line
        self.num_turns = num_turns
        self.nemitt_x = nemitt_x
        self.nemitt_y = nemitt_y

    def run(self):

        det_coefficients = self.line.get_amplitude_detuning_coefficients(
                                nemitt_x=self.nemitt_x, nemitt_y=self.nemitt_y,
                                num_turns=self.num_turns)

        out = {'d_xx': det_coefficients['det_xx'],
               'd_yy': det_coefficients['det_yy'],
               'd_xy': det_coefficients['det_xy'],
               'd_yx': det_coefficients['det_yx']}

        return out
    

for (sequence,mode) in SEQUENCES:
    print("-------------------------------------------")
    print(sequence+mode+'\n\n')
    tw, line, tab = load_lattice(sequence, mode)
    if "V24.3_GHC" in sequence:
        marker = "fd3a.1"
        if mode == 'z':
            nex = 6.34e-5
            ney = 1.70e-7
        elif mode == 't':
            nex = 5.61e-4
            ney = 5.71e-7
    elif "V24.3_LCC" in sequence:
        marker = "s.arc_uu"
        if mode == 'z':
            nex = 6.11e-5
            ney = 1.22e-7
        elif mode == 't':
            nex = 7.50e-4
            ney = 1.48e-6
    
    action = ActionMeasAmplDet(line=line, nemitt_x=nex, nemitt_y=ney, num_turns=256)
    
    #one octupole correction of det_xx
    #install_octupole(line.vars["octStr"], marker, 0)
    my_oct = xt.Multipole(length = 0.1, knl=[0,0,0,0]) #KNL 
    line.discard_tracker()
    line.insert_element('corr_oct', my_oct.copy(), at_s = tab['s',marker])
    line.build_tracker()
    
    line.vars["octStr"] = 0
    line.vars["skewOctStr"] = 0

    #print(line.vars["octStr"]._get_value())
    #exit()

    #line.ref['corr_oct'].knl[3] = line.vars['octStr']
    line['corr_oct'].knl[3] = line.vars['octStr']
    line['corr_oct'].ksl[3] = line.vars['skewOctStr']

    det_dict_base = line.get_amplitude_detuning_coefficients(nemitt_x = nex, nemitt_y = ney, num_turns = 256, a0_sigmas = 0.01, a1_sigmas = 0.1, a2_sigmas = 0.2)
    df = pd.DataFrame.from_dict(data = [det_dict_base], orient = 'columns')
    tw_base = line.twiss(method='4d') #Twiss with RF off
    df['ddQx'] = tw_base.ddqx
    df['ddQy'] = tw_base.ddqy
    df['Octupole Strength'] = 0
    print(df)


    line.vars["octStr"] = -16*math.pi*(det_dict_base["det_xx"])/(tw_base['betx','corr_oct']**2)
    line.vars["skewOctStr"] = 0
    #print(line.vars["octStr"]._get_value())
    print("------------------------------------------------------------------------------------------")

    opt = line.match(
            solve=False,
            method='4d',
            n_steps_max = 500,
            vary=[
                xt.VaryList(['octStr'], step=1e-2, tag='octFine'),
                xt.VaryList(['octStr'], step=1, tag='octCoarse', active=False),
                xt.VaryList(['skewOctStr'], step=1e-1, tag='skewOct', active=False)
                ],
            targets=[
                action.target('d_xx', 10, tol=0.1, tag='d_xx'),
                action.target('d_yy', 10, tol=0.1, tag='d_yy'),
                action.target('d_xy', 10, tol=200, tag='d_xy_coarse'),
                action.target('d_xy', 10, tol=1, tag='d_xy'),
                action.target('d_yx', 10, tol=200, tag='d_yx_coarse'),
                action.target('d_yx', 10, tol=1, tag='d_yx'),
                xt.TargetSet(ddqx=1, tol=1e-1, tag='ddqx', weight=1),
                xt.TargetSet(ddqy=1, tol=1e-1, tag='ddqy', weight=1)
                ]
            )
    print("------------------------------------------------------------------------------------------")
    #horizontal amplitude detuning
    opt.disable(target=True)
    opt.enable(target='d_xx')
    opt.solve()
    opt.target_status()
    opt.vary_status()
    check_non_linear(line,df,line['corr_oct'].knl[3],1)

    print("------------------------------------------------------------------------------------------")
    #vertical amplitude detuning
    line.vars["octStr"] = -16*math.pi*(det_dict_base["det_yy"])/(tw_base['bety','corr_oct']**2)
    print(line.vars["octStr"]._get_value())
    print(line['corr_oct'].knl)
    opt.clear_log() 
    opt.disable(target=True)
    opt.enable(target='d_yy')
    #opt.target_status()
    opt.solve()
    opt.target_status()
    opt.vary_status()
    check_non_linear(line,df,line['corr_oct'].knl[3],2)

    print("------------------------------------------------------------------------------------------")
    #vertical amplitude horizontal detuning dQx/dJy
    line.vars["octStr"] = 8*math.pi*(det_dict_base["det_xy"])/(tw_base['betx','corr_oct']*tw_base['bety','corr_oct'])
    print(line.vars["octStr"]._get_value())
    print(line['corr_oct'].knl)
    #coarse
    opt.clear_log() 
    opt.disable(vary = True)
    opt.disable(target=True)
    opt.enable(vary = 'octCoarse')
    opt.enable(target='d_xy_coarse')
    opt.target_status()
    opt.solve()
    opt.target_status()
    opt.vary_status()
    print("------------- start fine scan --------------------")
    #fine    
    opt.disable(vary=True)
    opt.disable(target = True)
    opt.enable(target='d_xy')
    opt.enable(vary='octFine')
    opt.target_status()
    opt.solve()
    opt.target_status()
    opt.vary_status()
    check_non_linear(line,df,line['corr_oct'].knl[3],3)
    print("------------------------------------------------------------------------------------------")
    #horizontal amplitude vertical detuning dQy/dJx
    line.vars["octStr"] = 8*math.pi*(det_dict_base["det_yx"])/(tw_base['betx','corr_oct']*tw_base['bety','corr_oct'])
    #print(line.vars["octStr"]._get_value())
    #print(line['corr_oct'].knl)
    
    #coarse
    opt.clear_log() 
    opt.disable(vary=True)
    opt.disable(target=True)
    opt.enable(vary= 'octCoarse')
    opt.enable(target='d_yx_coarse')
    opt.target_status()
    opt.solve()
    opt.target_status()
    opt.vary_status()
    print("------------- start fine scan --------------------")
    #fine    
    opt.disable(vary=True)
    opt.disable(target = True)
    opt.enable(target='d_yx')
    opt.enable(vary='octFine')
    opt.target_status()
    opt.solve()
    opt.target_status()
    opt.vary_status()
    check_non_linear(line,df,line['corr_oct'].knl[3],4)

    print("------------------------------------------------------------------------------------------")
    #horizontal Chromaticity
    line.vars["octStr"] = -4*math.pi*tw_base["ddqx"]/(tw_base['betx','corr_oct']*(tw_base["dx","corr_oct"]**2 - tw_base["dy","corr_oct"]**2))
    #print(line.vars["octStr"]._get_value())
    #print(line['corr_oct'].knl)

    opt.clear_log() 
    opt.disable(target=True)
    opt.disable(vary=True)
    opt.enable(target='ddqx')
    opt.enable(vary='octFine')
    opt.enable(vary='octCoarse')
    #opt.target_status()
    opt.target_status()
    opt.vary_status()
    opt.solve()
    opt.target_status()
    opt.vary_status()
    check_non_linear(line,df,line['corr_oct'].knl[3],5)
    
    print("------------------------------------------------------------------------------------------")
    #vertical chromaticity
    line.vars["octStr"] = -4*math.pi*tw_base["ddqy"]/(tw_base['betx','corr_oct']*(tw_base["dx","corr_oct"]**2 - tw_base["dy","corr_oct"]**2))
    #print(line.vars["octStr"]._get_value())
    #print(line['corr_oct'].knl)
    
    opt.clear_log() 
    opt.disable(target=True)
    opt.enable(target='ddqy')
    opt.target_status()
    opt.vary_status()
    opt.solve()
    opt.target_status()
    opt.vary_status()
    check_non_linear(line,df,line['corr_oct'].knl[3],6)

    for col in df.columns:
        df[col+"_abs"] = abs(df[col])
    print(df)

    # plotting graph
    fig,ax = plt.subplots(nrows=1,ncols=1,figsize=(18,9))
    df.plot.bar(y = ["det_xx_abs","det_yy_abs","det_xy_abs","det_yx_abs","ddQx_abs","ddQy_abs"], logy = True,ax=ax, width=0.75)
    #df.plot.bar(y = ["det_xy_abs","det_yx_abs"], logy = True,ax=ax, width=0.75)
    ax.set_xticks(ticks = df.index, labels=round(df["Octupole Strength"],2))
    ax.set_xlabel("Corrector Octupole KNL", fontsize=25)
    ax.set_title(sequence+" "+mode+" with a corrector octupole located at "+marker)
    ax.set_ylabel("Amplitude of Nonlinear Optics Terms", fontsize=25)
    ax.tick_params(axis='x', labelrotation=45)
    ax.axhline(y=1)
    ax.axhline(y=10)
#    ax.set_ylim([1e-4,1e7])
    plt.savefig("Outputdata/figures/oct_corr_"+sequence+mode+".png")
    #plt.show()


    #exit()
