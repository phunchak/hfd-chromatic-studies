import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import xfields as xf
from cpymad.madx import Madx
import matplotlib.pyplot as plt
import pandas as pd
import tfs
from scipy.constants import e as qe
from scipy.constants import m_p
import math
import itertools
import re

#load lattice parameters json
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))

def load_lattice(sequence, mode):
    # load from json
    with open('lattices_xsuite/'+sequence+mode+'_lattice.json', 'r') as fid:
        loaded_dct = json.load(fid)
    
    line = xt.Line.from_dict(loaded_dct)
    
    #positron for z lattice
    energy = REF_FILE[mode]["ENERGY"]*1e9
    line.particle_ref = xt.Particles(mass0=xt.ELECTRON_MASS_EV, q0=1, energy0=REF_FILE[mode]["ENERGY"]*1e9)
    line.build_tracker()
    # We consider a case in which all RF cavities are off
    tab = line.get_table()
    tab_cav = tab.rows[tab.element_type == 'Cavity']
    for nn in tab_cav.name:
        line[nn].voltage = 0
    
    tw = line.twiss(method='4d') #Twiss with RF off 
    return tw, line


def tune_chroma_match(sequence, mode,   #identity of lattice to work on
        twiss,                          #previously run twiss for reference tunes (no default value)
        line,                           #
        tuneMatch = 0,                  #set to 1 to run tune matching
        deltaQX = 0, deltaQY = 0,       #desired change in horiz. and vert. tunes
        chromaMatch = 0,                #set to 1 to run chromaticity matching
        deltaDQX = 0, deltaDQY = 0):    #desired change in chromaticity
    
    #make matching knobs
    qfamilies = REF_FILE["VERSION"][sequence]["QUADS"]
    sf_families = REF_FILE["VERSION"][sequence]["MATCH_SF_SEXTS"]
    sd_families = REF_FILE["VERSION"][sequence]["MATCH_SD_SEXTS"]

    line.vars['sf_knob'] = 0 #initialize sextupole delta knobs
    line.vars['sd_knob'] = 0 

    # apply sextupole delta knobs to the matching sextupoles
    # I don't believe this includes previous dependencies 
    for i in range(len(sf_families)):
        line.vars[sf_families[i]] = line.vars[sf_families[i]]._get_value() + line.vars['sf_knob']
    for i in range(len(sd_families)):
        line.vars[sd_families[i]] = line.vars[sd_families[i]]._get_value() + line.vars['sd_knob']
    
    # Match tunes and chromaticities to assigned values
    opt = line.match(
        solve = False, # just setting up optimization call the solve later
        method='4d', # <- passed to twiss
        vary=[ xt.VaryList(qfamilies, step=1e-8, tag='quad'),
            xt.VaryList(['sf_knob','sd_knob'], step=1e-4, tag='sext'),
        ],
        targets = [
            xt.TargetSet(qx=(tw.qx + deltaQX), qy=(tw.qy + deltaQY), tol=1e-6, tag='tune', weight=1),
            xt.TargetSet(dqx=(tw.dqx + deltaDQX), dqy=(tw.dqy +deltaDQY), tol=1e-6, tag='chroma', weight=1),
        ])
    # view optimization setup
    #opt.show()
    #print(opt.target_status())

    # choose optimizatin targets
    opt.disable_all_targets();
    opt.disable_all_vary();
    if tuneMatch == 1:
        opt.enable_vary(tag='quad')
        opt.enable_targets(tag='tune')
    if chromaMatch == 1:
        opt.enable_vary(tag='sext')
        opt.enable_targets(tag='chroma')
    print(opt.target_status())
    
    #solve optimization
    opt.solve()
    # Inspect optimization log
    opt.log()

    # Inspect optimization outcome
    opt.target_status()
    opt.vary_status()

    # Get knob values after optimization
    knobs_after_match = opt.get_knob_values()

    # Get knob values before optimization
    knobs_before_match = opt.get_knob_values(iteration=0)


for (sequence,mode) in SEQUENCES:
    tw, line = load_lattice(sequence, mode)
    tune_chroma_match(sequence, mode, twiss = tw, line = line, tuneMatch = 0, deltaQX = 0.1, deltaQY = 0.2, chromaMatch = 1, deltaDQX = 0.2, deltaDQY = 0.2)

