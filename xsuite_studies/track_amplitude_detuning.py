import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import xfields as xf
from cpymad.madx import Madx
import matplotlib.pyplot as plt
import pandas as pd
import tfs
from scipy.constants import e as qe
from scipy.constants import m_p
import math
import itertools
import re
import os


#load lattice parameters json
with open('../lattices/lattice_parameters.json', 'r') as file:
    REF_FILE = json.load(file)
#make list of lattices
SEQUENCES = list(itertools.product(REF_FILE['VERSION'].keys(),REF_FILE['OPERATION_MODES'].keys()))

def load_lattice(sequence, mode):
    # load from json
    with open('lattices_xsuite/'+sequence+mode+'_lattice.json', 'r') as fid:
        loaded_dct = json.load(fid)
    
    line = xt.Line.from_dict(loaded_dct)
    #positron for z lattice
    energy = REF_FILE["VERSION"][sequence][mode]["ENERGY"]*1e9
    line.particle_ref = xt.Particles(mass0=xt.ELECTRON_MASS_EV, q0=1, energy0=energy)
    ## Choose a context
    context = xo.ContextCpu()         # For CPU
    # context = xo.ContextCupy()      # For CUDA GPUs
    # context = xo.ContextPyopencl()  # For OpenCL GPUs
    # We consider a case in which all RF cavities are off
    line.build_tracker(_context=context)
    tab = line.get_table()
    #print(tab.rows[tab.element_type == 'Marker'])
    tab_cav = tab.rows[tab.element_type == 'Cavity']
    for nn in tab_cav.name:
        line[nn].voltage = 0
    
    tw = line.twiss(method='4d') #Twiss with RF off 

    return tw, line, tab

def tracking_amplitude(line, mode, tw, Nsigmax, Nsigmay,marker,n_turns):
    ## Build particle object on context
    
    element_index = marker
    #phi_x = np.random.uniform(-math.pi, math.pi, n_part)
    phi_x = 0
    beamsize_x = Nsigmax*np.sqrt(REF_FILE["VERSION"][sequence][mode]["EMIT_X"]*tw.rows[element_index].betx)
    Jx = 0.5*(beamsize_x**2)/tw.rows[element_index].betx
    x = np.sqrt(2*Jx*tw.rows[element_index].betx)*np.cos(phi_x)
    px = -np.sqrt(2*Jx/tw.rows[element_index].betx)*(np.sin(phi_x)+tw.rows[element_index].alfx*np.cos(phi_x))

    #phi_y = np.random.uniform(-math.pi, math.pi, n_part)
    phi_y = 0
    beamsize_y = Nsigmay*np.sqrt(REF_FILE["VERSION"][sequence][mode]["EMIT_Y"]*tw.rows[element_index].bety)
    Jy = 0.5*(beamsize_y**2)/tw.rows[element_index].bety
    y = np.sqrt(2*Jy*tw.rows[element_index].bety)*np.cos(phi_y)
    py = -np.sqrt(2*Jy/tw.rows[element_index].bety)*(np.sin(phi_y)+tw.rows[element_index].alfy*np.cos(phi_y))
    
    line.discard_tracker()
    monitor_start_loc = xt.ParticlesMonitor(start_at_turn=0, stop_at_turn=n_turns, num_particles=1)
    line.insert_element(index=marker, element=monitor_start_loc, name='mymon')
    line.build_tracker(_context=xo.ContextCpu())
    particles = xp.Particles(x=x,px=px,y=y,py=py,mass0=xt.ELECTRON_MASS_EV, q0=1, energy0=REF_FILE["VERSION"][sequence][mode]["ENERGY"]*1e9,at_element=(len(tw.rows[:marker])-1))

    
   # print(particles.x)
   # print(particles.px)
   # print(particles.y)
   # print(particles.py)
    line.track(particles, num_turns=n_turns, ele_start=(len(tw.rows[:marker])-1), ele_stop=(len(tw.rows[:marker])-1),  turn_by_turn_monitor=True)
    #print(monitor_start_loc) 
    return monitor_start_loc


    # Reference mass, charge, energy are taken from the reference particle.
    # Particles are allocated on the context chosen for the line.


for (sequence,mode) in SEQUENCES:
    print("-------------------------------------------")
    print(sequence+mode+'\n\n')
    n_turns = 10000
    tw, line, tab = load_lattice(sequence, mode)
    if "V24.3_GHC" in sequence:
        marker = "fd3a.1"
        if mode == 'z':
            nex = 6.34e-5
            ney = 1.70e-7
        elif mode == 't':
            nex = 5.61e-4
            ney = 5.71e-7
    elif "V24.3_LCC" in sequence:
        marker = "s.arc_uu"
        if mode == 'z':
            nex = 6.11e-5
            ney = 1.22e-7
        elif mode == 't':
            nex = 7.50e-4
            ney = 1.48e-6
    
    det_dict = line.get_amplitude_detuning_coefficients(nemitt_x = nex, nemitt_y = ney, num_turns = 256, a0_sigmas = 0.01, a1_sigmas = 0.1, a2_sigmas = 0.2)
    print(det_dict)
    print(tw.rows[marker])
    #Nsigmax = 10
    #Nsigmay = 0
#    for Nsigmax in range(1,11,1):
#        for Nsigmay in [1]:
#            print(Nsigmax)
#            print(Nsigmay)
#            lastTrack = tracking_amplitude(line, mode, tw, Nsigmax, Nsigmay,marker, n_turns)
#            with open("Outputdata/"+sequence+mode+"/particles_nsig_X_"+"{:02d}".format(Nsigmax)+"_Y_"+"{:02d}".format(Nsigmay)+".json", 'w') as fid:
#                json.dump(lastTrack.to_dict(), fid, cls=xo.JEncoder)
#    for Nsigmax in [1]:
#        for Nsigmay in range(1,11,1):
#            print(Nsigmax)
#            print(Nsigmay)
#            lastTrack = tracking_amplitude(line, mode, tw, Nsigmax, Nsigmay,marker, n_turns)
#            with open("Outputdata/"+sequence+mode+"/particles_nsig_X_"+"{:02d}".format(Nsigmax)+"_Y_"+"{:02d}".format(Nsigmay)+".json", 'w') as fid:
#            #with open("Outputdata/"+sequence+mode+"/particles_nsig_X_"+str(Nsigmax)+"_Y_"+str(Nsigmay)+".json", 'w') as fid:
#                json.dump(lastTrack.to_dict(), fid, cls=xo.JEncoder)


